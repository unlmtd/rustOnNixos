# Fragments
------------

> Hi list,
> 
> # Motivation
> 
> I saw an issue on github the other day about Rust packaging #23291. I
> have faced the problem that Rust nightly version was not enough for
> building rustc out of the repository.
> 
> Rust nightly is probably changing too frequently to be maintained
> within Nixpkgs #8697.  Also, Firefox developer, I will need to follow
> Rust versions as soon as they are packaged and updated in Firefox.
> 
> The recommended way for developing Firefox is to periodically run
> rustup (every ~6 weeks), which is sad from my point of view.  As I
> rely on nixpkgs-mozilla [1] repository to generate the build
> environment I use, I decided to tackle the issue inherent of having
> recent version of rustc & cargo.
> 
> # What
> 
> I created an overlay which mimic rustup behaviour, and allow you to
> spawn a nix-shell with the latest nightly, beta, or stable version of
> rustc & cargo.
> 
> To use this overlay, checkout nixpkgs-mozilla [1], and create a
> symbolic link to the file rust-overlay.nix [2] in the
> ~/.config/nixpkgs/overlays
> 
>   $ cd ~/
>   $ git clone https://github.com/mozilla/nixpkgs-mozilla.git
>   $ mkdir -p  ~/.config/nixpkgs/overlays
>   $ ln -s ~/nixpkgs-mozilla/rust-overlay.nix
> ~/.config/nixpkgs/overlays/rust-overlay.nix
> 
> Once installed, you can install the latest versions with the following commands:
> 
>   $ nix-env -Ai nixos.rustChannels.stable.rust
> 
> Or use this attribute to build a nix-shell environment which pull the
> latest version of rust for you when you enter it.
> 
> You can substitute the "stable" channel by "nightly" and "beta", or
> use the function provided by this overlay to pull a version based on a
> build date.
> 
> # How
> 
> This overlay should auto-update it-self as if you were running rustup
> each time you go through the rustChannels attributes.  It works by
> using the fetchurl builtin function to pull the same file as rustup do
> through https.
> 
> The *.toml manifest file is then parsed (yes, in Nix [3]) to extract
> the sha256 and the location of all the packages indexed by the
> manifest file.  Then, some logic is used [2] to convert the *.toml
> file into proper derivations which are used to pull the prebuilt
> binaries and to change the interpreter of the binaries using patchelf.
> 
> If you encounter issue, feel free to report them on the
> nixpkgs-mozilla repository [1], and to fix them as well ;)
> 
> Have fun and enjoy.
> 
> #23291: https://github.com/NixOS/nixpkgs/issues/23291
> #8697: https://github.com/NixOS/nixpkgs/issues/8697
> [1] https://github.com/mozilla/nixpkgs-mozilla
> [2] https://github.com/mozilla/nixpkgs-mozilla/blob/master/rust-overlay.nix
> [3] https://github.com/mozilla/nixpkgs-mozilla/blob/master/lib/parseTOML.nix

Source: https://www.mail-archive.com/nix-dev@lists.science.uu.nl/msg33296.html
